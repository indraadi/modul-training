package com.bca.example.transfer.util;

import com.bca.example.transfer.model.ExecuteTransferRequest;
import com.bca.example.transfer.repository.TransferHistoryEntity;
import com.bca.example.transfer.repository.TransferHistoryRepository;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

import static java.util.Arrays.asList;

@Component
public class TransferUtil {

    private TransferHistoryRepository transferHistoryRepository;

    public TransferUtil(TransferHistoryRepository transferHistoryRepository) {
        this.transferHistoryRepository = transferHistoryRepository;
    }

    @Transactional
    public TransferHistoryEntity insertTransferHistory(ExecuteTransferRequest executeTransferRequest, String reffNo) {
        return transferHistoryRepository.save(TransferHistoryEntity.builder()
                .amount(new BigDecimal(executeTransferRequest.getAmount()))
                .beneficiaryAccountNumber(executeTransferRequest.getBeneficiaryAccountNumber())
                .transactionTime(LocalDateTime.now())
                .sourceAccountNumber(executeTransferRequest.getSourceAccountNumber())
                .desc(executeTransferRequest.getDescription())
                .referenceNumber(reffNo)
                .build());
    }

    @Transactional
    public void updateFinalStatus(TransferHistoryEntity entity) {
        transferHistoryRepository.save(entity);
    }

    public String generateReferenceNumber() {
        return (new Random().nextInt(100000000)) + "";
    }

    public List<TransferHistoryEntity> getAllHistory() {
        return transferHistoryRepository.findAll();
    }

}
