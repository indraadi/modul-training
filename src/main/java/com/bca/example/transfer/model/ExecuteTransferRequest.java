package com.bca.example.transfer.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ExecuteTransferRequest {

    private String sourceAccountNumber;
    private String amount;
    private String beneficiaryAccountNumber;
    private String description;
}
