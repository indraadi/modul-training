package com.bca.example.transfer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TransferHistoryRepository extends JpaRepository<TransferHistoryEntity, String> {
}
