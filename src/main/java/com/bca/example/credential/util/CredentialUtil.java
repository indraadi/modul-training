package com.bca.example.credential.util;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class CredentialUtil {


    public static String getSourceClientID(HttpHeaders headers) {
        return getHeaderValue(headers, "client-id");
    }

    public static String getHeaderValue(HttpHeaders headers, String key) {
        return headers.getFirst(key) == null ? "" : headers.getFirst(key);
    }


}
