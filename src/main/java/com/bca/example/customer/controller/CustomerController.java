package com.bca.example.customer.controller;

import com.bca.example.customer.model.CustomerDetailResponse;
import com.bca.example.customer.service.CustomerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    private CustomerService customerServices;

    public CustomerController(CustomerService customerServices) {
        this.customerServices = customerServices;
    }

    @GetMapping("/all-customer")
    public List<CustomerDetailResponse> getAllCustomer() {
        return customerServices.getDetailCustomerAll();
    }

    @GetMapping("/details")
    public CustomerDetailResponse getSpecificCustomer(
            @RequestHeader(value = "account-number") String accountNumber
    ) {
        return customerServices.getDetailCustomerByAccountNumber(accountNumber);
    }

}
