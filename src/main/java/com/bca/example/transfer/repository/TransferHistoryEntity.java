package com.bca.example.transfer.repository;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TRANSFER_HISTORY")
public class TransferHistoryEntity {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private String id;

    private String sourceAccountNumber;
    private String beneficiaryAccountNumber;

    private BigDecimal amount;

    private String desc;
    private String referenceNumber;

    private LocalDateTime transactionTime;
    private String status;

    private String errorMessage;

}
