package com.bca.example.transfer.model;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
public class TransferHistoryResponse {

    private String sourceAccountNumber;
    private String beneficiaryAccountNumber;

    private String amount;

    private String desc;
    private String referenceNumber;

    private LocalDateTime transactionTime;
    private String status;

    private String errorMessage;
}
