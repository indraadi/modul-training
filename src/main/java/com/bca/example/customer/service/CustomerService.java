package com.bca.example.customer.service;

import com.bca.example.customer.model.CustomerDetailResponse;
import com.bca.example.customer.repository.CustomerEntity;
import com.bca.example.customer.repository.CustomerRepository;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CustomerService {

    private CustomerRepository customerRepo;

    public CustomerService(CustomerRepository customerRepo) {
        this.customerRepo = customerRepo;
    }

    public List<CustomerDetailResponse> getDetailCustomerAll() {
        return customerRepo.findAll()
                .stream()
                .map(x ->
                        CustomerDetailResponse.builder()
                                .customerName(x.getCustomerName())
                                .accountNumber(x.getAccountNumber())
                                .balance(x.getBalance().toString())
                                .build()
                ).collect(Collectors.toList());
    }

    public CustomerDetailResponse getDetailCustomerByAccountNumber(String accountNumber) {
        Optional<CustomerEntity> customer = customerRepo.findByAccountNumber(accountNumber);

        if (customer.isPresent()) {
            CustomerEntity customerEntityDataPresent = customer.get();
            return CustomerDetailResponse.builder()
                    .accountNumber(customerEntityDataPresent.getAccountNumber())
                    .customerName(customerEntityDataPresent.getCustomerName())
                    .balance(customerEntityDataPresent.getBalance().toString())
                    .build();
        }
        return CustomerDetailResponse.builder().build();

    }

    public void checkBalance(String accountNumber, BigDecimal amount) throws Exception {
        if (!(getCustomerEntity(accountNumber).getBalance().compareTo(amount) >= 0))
            throw new Exception("Saldo tidak" +
                    " cukup");
    }

    private CustomerEntity getCustomerEntity(String accountNumber) throws Exception {
        Optional<CustomerEntity> customer = customerRepo.findByAccountNumber(accountNumber);

        if (!customer.isPresent()) throw new Exception("Account Number tidak ditemukan");
        return customer.get();
    }

    public String debetCredit(String sourceAccountNumber, String beneficiaryAccountNumber,
                              String amount) throws Exception {

        CustomerEntity sourceCustomerEntity = this.getCustomerEntity(sourceAccountNumber);
        CustomerEntity beneficiaryCustomerEntity = this.getCustomerEntity(beneficiaryAccountNumber);

        BigDecimal remainingBalance = sourceCustomerEntity.getBalance().subtract(new BigDecimal(amount));
        sourceCustomerEntity.setBalance(remainingBalance);
        customerRepo.save(sourceCustomerEntity);

        beneficiaryCustomerEntity.setBalance(beneficiaryCustomerEntity.getBalance().add(new BigDecimal(amount)));
        customerRepo.save(beneficiaryCustomerEntity);

        return remainingBalance.toString();
    }
}
