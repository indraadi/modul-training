package com.bca.example.transfer.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ExecuteTransferResponse {

    private String referenceNumber;
    private String status;
    private String remainingBalance;
    private String errorMessage;

}
