package com.bca.example.customer.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerDetailResponse {
    private String accountNumber;
    private String customerName;
    private String balance;
}
