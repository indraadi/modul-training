package com.bca.example.transfer.services;

import com.bca.example.credential.util.CredentialUtil;
import com.bca.example.customer.service.CustomerService;
import com.bca.example.transfer.model.ExecuteTransferRequest;
import com.bca.example.transfer.model.ExecuteTransferResponse;
import com.bca.example.transfer.model.TransferHistoryResponse;
import com.bca.example.transfer.repository.TransferHistoryEntity;
import com.bca.example.transfer.util.TransferUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;


@Service
public class TransferServices {

    private TransferUtil transferUtil;

    private CustomerService customerService;

    private String validChannelId;

    public TransferServices(TransferUtil transferUtil, CustomerService customerService,
                            @Value("${service.config.valid-channel-id}") final String validChannelId) {
        this.transferUtil = transferUtil;
        this.customerService = customerService;
        this.validChannelId = validChannelId;
    }


    public ExecuteTransferResponse doTransfer(HttpHeaders headers, ExecuteTransferRequest request) {
        try {
            if (!validChannelId.equals(CredentialUtil.getSourceClientID(headers)))
                throw new Exception("Anda tidak berhak mengakses services");

            customerService.checkBalance(request.getSourceAccountNumber(),
                    new BigDecimal(request.getAmount()));

            String refNo = transferUtil.generateReferenceNumber();
            TransferHistoryEntity entity = transferUtil.insertTransferHistory(request,
                    refNo);
            String remainingBalance;
            try {
                remainingBalance =
                        customerService.debetCredit(request.getSourceAccountNumber(),
                                request.getBeneficiaryAccountNumber(), request.getAmount());
            } catch (Exception e) {
                entity.setStatus("Failed");
                entity.setErrorMessage(e.getMessage());
                transferUtil.updateFinalStatus(entity);
                throw e;
            }
            // update db
            entity.setStatus("Success");
            transferUtil.updateFinalStatus(entity);
            return ExecuteTransferResponse.builder()
                    .status("Success")
                    .referenceNumber(refNo)
                    .remainingBalance(remainingBalance)
                    .build();
        } catch (Exception ex) {
            return ExecuteTransferResponse.builder()
                    .status("Failed")
                    .errorMessage(ex.getMessage())
                    .build();
        }
    }

    public TransferHistoryResponse getAllHistory() {
        return null;
    }
}
