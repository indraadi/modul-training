package com.bca.example.transfer.services;

import com.bca.example.transfer.model.TransferHistoryResponse;
import com.bca.example.transfer.repository.TransferHistoryEntity;
import com.bca.example.transfer.util.TransferUtil;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;

@Service
public class TransferHistoryService {

    private TransferUtil transferUtil;

    public TransferHistoryService(TransferUtil transferUtil) {
        this.transferUtil = transferUtil;
    }

    public List<TransferHistoryResponse> getAllHistory() {

        List<TransferHistoryEntity> allHistory = transferUtil.getAllHistory();
        System.out.println(allHistory);
        if (allHistory.size() == 0) return asList();
        return allHistory.stream().map(
                x -> TransferHistoryResponse.builder()
                        .status(x.getStatus())
                        .amount(x.getAmount().toString())
                        .desc(x.getDesc())
                        .beneficiaryAccountNumber(x.getBeneficiaryAccountNumber())
                        .referenceNumber(x.getReferenceNumber())
                        .sourceAccountNumber(x.getSourceAccountNumber())
                        .transactionTime(x.getTransactionTime())
                        .errorMessage(x.getErrorMessage())
                        .build()
        ).collect(Collectors.toList());
    }
}

