package com.bca.example.transfer.controller;

import com.bca.example.transfer.model.ExecuteTransferRequest;
import com.bca.example.transfer.model.ExecuteTransferResponse;
import com.bca.example.transfer.services.TransferServices;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("transfer")
public class TransferController {

    private TransferServices transferServices;

    public TransferController(TransferServices transferServices) {
        this.transferServices = transferServices;
    }

    @PostMapping("/execute")
    public ExecuteTransferResponse execute(
            @RequestHeader HttpHeaders headers,
            @RequestBody ExecuteTransferRequest request
    ) {
        return transferServices.doTransfer(headers, request);
    }

}
