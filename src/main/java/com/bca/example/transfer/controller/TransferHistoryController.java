package com.bca.example.transfer.controller;

import com.bca.example.transfer.model.TransferHistoryResponse;
import com.bca.example.transfer.services.TransferHistoryService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/history")
public class TransferHistoryController {

    private TransferHistoryService transferHistoryService;

    public TransferHistoryController(TransferHistoryService transferHistoryService) {
        this.transferHistoryService = transferHistoryService;
    }

    @GetMapping("/execute")
    public List<TransferHistoryResponse> getAllHistory() {
        return transferHistoryService.getAllHistory();
    }

}
